# CounterBot

CounterBot is a Telegram bot that provides statistics about messages posted by users in a group. It can calculate the total number of messages and filter messages based on a given date range.

## Features

- Calculate the total number of messages in a group.
- Filter messages based on a given date range (e.g., week, month, or custom date).

## Usage

To use CounterBot, add it to your Telegram group and interact with it using the following commands:

- `/sana <date>`: Get statistics about messages sent by a specific user.
  - `<date>`: Date range for filtering messages. You can specify 'week', 'month', or a custom date in various formats (e.g., 'YYYY-MM-DD').


## Getting Started

To run CounterBot locally, follow these steps:

1. Clone this repository:

```commandline
https://gitlab.com/leaderman0114/counter-bot.git
```

2. Install the required dependencies:

```commandline
cd counter-bot
pip install -r requirements.txt
```

3. Create a .env file in the project directory and add the following environment variables:
```text
API_ID=your_api_id
API_HASH=your_api_hash
PHONE_NUMBER=your_phone_number
ADMIN_IDS=admin_ids  # with comma
```

4. Replace your_api_id, your_api_hash, your_phone_number, admin_ids with your own values obtained from the Telegram API website.

5. Run the bot:
```commandline
python counter_bot.py
```

Contributing
If you encounter any issues or have suggestions for improvements, feel free to open an issue or submit a pull request.
