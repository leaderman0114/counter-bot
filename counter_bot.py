from dateutil.relativedelta import relativedelta
from pyrogram.enums import ParseMode
from pyrogram import Client, filters
from pyrogram.types import Message
from dotenv import load_dotenv
from time import sleep
import dateparser
import datetime
import logging
import os


# Load environment variables from .env file
load_dotenv()

logger = logging.getLogger(__name__)

# Get API ID and API hash from environment variables
api_id = os.getenv("API_ID")
api_hash = os.getenv("API_HASH")
phone_number = os.getenv("PHONE_NUMBER")
admin_ids = os.getenv("ADMIN_IDS").split(',')


# Create a Pyrogram client
app = Client("Counter", api_id=api_id, api_hash=api_hash, phone_number=phone_number)


def format_stats(data: dict, limit_date):
    username = f"@{data['username']}" if data['username'] else "нет имени пользователя"
    formatted_stats = f"""
ℹ️ Группа: **{data['group_name']}**
📅 Срок: **{limit_date}**
🆔 ID: `{data['id']}`
👤 Имя: **{data['first_name']}**
📎 Имя пользователя: {username }
📩 Общее сообщений: **{data['total_messages']}**
"""
    return formatted_stats


last_commands = []


# Define a handler for the /sana command
@app.on_message(filters.command("sana") & filters.group)
async def calculate(bot: Client, message: Message):
    if not message.from_user:
        return False
    if str(message.from_user.id) not in admin_ids:
        return False
    limit_date = None
    if len(message.command) < 2:
        return await bot.send_message(chat_id=message.from_user.id, text='Ошибка команды')
    elif len(message.command) == 2:
        try:
            limit_date_str = message.command[1]
            if limit_date_str == 'week':
                limit_date = datetime.date.today() - datetime.timedelta(weeks=1)
            elif limit_date_str == 'month':
                limit_date = datetime.date.today() - relativedelta(months=1)
            else:
                limit_date = dateparser.parse(limit_date_str).date()
        except Exception as e:
            logger.error(e)
            return await bot.send_message(message.from_user.id, 'формат даты неверен')
    command_str = f"{message.from_user.id}_{'_'.join(message.command)}"
    if command_str in last_commands:
        return await bot.send_message(message.from_user.id, 'уже в процессе')
    await bot.send_message(message.from_user.id, f'Начни считать')
    last_commands.append(command_str)
    try:
        print(f'{command_str} starting...')
        message_counts = {}

        # Get the message count for the specified user
        chat_id = message.chat.id

        flag = True
        offset_date = datetime.datetime.now()
        offset_id = 0
        while flag:
            print(f'{command_str} | {limit_date} | {offset_id} | {offset_date}')
            messages = bot.get_chat_history(chat_id=chat_id, limit=200, offset_id=offset_id)

            async for gr_message in messages:
                if not gr_message.from_user:
                    continue
                if limit_date:
                    if gr_message.date.date() < limit_date:
                        flag = False
                        break
                user_data = message_counts.get(gr_message.from_user.id)
                if not user_data:
                    message_counts[gr_message.from_user.id] = {
                        'id': gr_message.from_user.id,
                        'first_name': gr_message.from_user.first_name,
                        'username': gr_message.from_user.username,
                        'group_name': gr_message.chat.title,
                        'total_messages': 1,
                    }
                else:
                    message_counts[gr_message.from_user.id]['total_messages'] += 1
                offset_date = gr_message.date
                offset_id = gr_message.id
            # offset += 100
            sleep(0.2)

        for user_id, msg_data in message_counts.items():
            await bot.send_message(
                chat_id=message.from_user.id, text=format_stats(msg_data, limit_date), parse_mode=ParseMode.MARKDOWN)
            sleep(0.5)
        last_commands.remove(command_str)
        return await bot.send_message(chat_id=message.from_user.id, text='Все кончено')
    except Exception as e:
        if command_str in last_commands:
            last_commands.remove(command_str)
        logger.error(e)
        print(e)
        return await bot.send_message(message.from_user.id, "Неизвестная проблема, свяжитесь с разработчиком")


# Run the bot
app.run()
